<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class CategoryController extends Controller
{
    /**
     * [addCategory description]  Tạo mới danh mục
     */
    public function addCategory()
    {
        $data['cates'] = DB::table('category')->get();
        return view('BackEnd/Pages/Cates/add', $data);
    }
    /**
     * [postAddCategory description]  Xử lý thêm mới danh mục
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postAddCategory(Request $request)
    {
        $name = trim($request->name);
        if ($name == '') {
            return back()->with('error', 'Tên danh mục không được để trống');
        }
        if (strlen($name) < 3) {
            return back()->with('error', 'Tên danh mục không được ít hơn 3 ký tự');
        }

        $isCat = DB::table('category')->where('cat_slug', str_slug($name))->count();

        if($isCat > 0) {
            return back()->with('error', 'Danh mục đã tồn tại');
        }

        $idCate = DB::table('category')->insertGetId([
            'cat_name'      => $name,
            'cat_slug'      => str_slug($name),
            'cat_parent_id' => $request->danh_muc_cap_cha
        ]);

        if ($idCate) {
            return redirect('mx-admin/danh-muc');
        }
        else {
            return back()->with('error', 'Tạo mới thất bại, vui lòng kiểm tra lại');
        }
    }
    /**
     * [category description]  Danh sách Danh mục
     * @return [type] [description]
     */
    public function category()
    {
        $data['cates'] = DB::table('category')->get();
        return view('BackEnd/Pages/Cates/list', $data);
    }

    public function deleteCategory($cate_id)
    {
        $isCat = DB::table('category')->where('cat_id', $cate_id)->get();
        if($isCat[0]->cat_parent_id == 0)
        {
            $getCateParent = DB::table('category')->where('cat_parent_id', $isCat[0]->cat_id)->get();
            if($getCateParent->count() > 0)
            {
                $getCateParent = DB::table('category')->where('cat_parent_id', $isCat[0]->cat_id)->update(['cat_parent_id' => 0]);
            }
            DB::table('category')->where('cat_id', $cate_id)->delete();
        }
        else
        {
            DB::table('category')->where('cat_id', $cate_id)->delete();
        }
        return back()->with('success', 'Đã xóa danh mục '.$isCat[0]->cat_name);
    }
    
    /**
     * [editCategory description]  Sửa Danh mục
     * @param  [type] $cate_id [description]
     * @return [type]          [description]
     */
    public function editCategory($cate_id)
    {
        $data['get_cate']   = DB::table('category')->where('cat_id', $cate_id)->get();
        $data['cates']      = DB::table('category')->whereNotIn('cat_id', [ $cate_id ])->get();
        return view('BackEnd/Pages/Cates/edit', $data);
    }
    /**
     * [postEditCategory description]  Xử lý Sửa Danh mục
     * @param  Request $request [description]
     * @param  [type]  $cate_id [description]
     * @return [type]           [description]
     */
    public function postEditCategory(Request $request, $cate_id)
    {
        $name = trim($request->name);
        if ($name == '') {
            return back()->with('error', 'Tên danh mục không được để trống');
        }
        if (strlen($name) < 3) {
            return back()->with('error', 'Tên danh mục không được ít hơn 3 ký tự');
        }
        /** Kiểm tra xem tên danh mục đã tồn tại chưa **/
        $isCat = DB::table('category')->whereNotIn('cat_id', [ $cate_id ])->where('cat_slug', str_slug($name))->count();
        if($isCat > 0) {
            return back()->with('error', 'Danh mục đã tồn tại');
        }

        $idCate = DB::table('category')->where('cat_id', $cate_id)->update([
            'cat_name'      => $name,
            'cat_slug'      => str_slug($name),
            'cat_parent_id' => $request->danh_muc_cap_cha
        ]);
        return redirect('mx-admin/danh-muc');
    }
 
}
