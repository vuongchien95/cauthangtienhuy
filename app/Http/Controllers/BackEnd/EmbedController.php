<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class EmbedController extends Controller
{   
    public function settingEmCode()
    {
    	return view('BackEnd/Pages/EmbedCode/setting');
    }
    public function postEmbedCode(Request $request)
    {
        $header = trim($request->ma_nhung_header);
        $isHd = DB::table('embed_code')->where('em_option', 1)->count();
        if($isHd > 0)
        {
            DB::table('embed_code')->where('em_option', 1)->update([
                'em_code'   => $header,
                'em_option' => 1
            ]);
        }
        else
        {
            DB::table('embed_code')->insertGetId([
                'em_code'   => $header,
                'em_option' => 1
            ]);
        }

        $body = trim($request->ma_nhung_body);
        $isBd = DB::table('embed_code')->where('em_option', 2)->count();
        if($isBd > 0)
        {
            DB::table('embed_code')->where('em_option', 2)->update([
                'em_code'   => $body,
                'em_option' => 2
            ]);
        }
        else
        {
            DB::table('embed_code')->insertGetId([
                'em_code'   => $body,
                'em_option' => 2
            ]);
        }
        
        $footer = trim($request->ma_nhung_footer);
        $isFt = DB::table('embed_code')->where('em_option', 3)->count();
        if($isFt > 0)
        {
            DB::table('embed_code')->where('em_option', 3)->update([
                'em_code'   => $footer,
                'em_option' => 3
            ]);
        }
        else
        {
            DB::table('embed_code')->insertGetId([
                'em_code'   => $footer,
                'em_option' => 3
            ]);
        }

        return back();
    }
}
