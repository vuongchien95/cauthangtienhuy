<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth, DB;

class NewsController extends Controller
{
    public function creatNews()
    {
    	$data['cates'] = DB::table('category')->where('cat_parent_id', 0)->get();
    	return view('BackEnd/Pages/News/add', $data);
    }
    
    public function postCreatNews(Request $request)
    {
    	$getNews = DB::table('news')->where('ne_slug', str_slug($request->title))->count();
    	if($getNews > 0)
    	{
    		return back()->with('error', 'Tiêu đề bài viết đã tồn tại');
    	}
    	$arrInsert 	 = [];
    	$arrInsert['ne_title'] 	  	= trim($request->title);
    	$arrInsert['ne_slug'] 	  	= str_slug($request->title);
    	$arrInsert['ne_cate'] 	  	= 0;
        $arrInsert['ne_des']        = trim($request->des);
    	$arrInsert['ne_content'] 	= trim($request->content);
    	$arrInsert['ne_tags'] 		= trim($request->tags);
    	$arrInsert['ne_keyword'] 	= trim($request->keyword);
    	$arrInsert['ne_description']= trim($request->description);
        $arrInsert['ne_user_id']    = Auth::user()->id;
    	$arrInsert['ne_type'] 	    = 0;
    	if($imgAvatar = $request->file('avatar'))
        {
            $nameAvatar = rand().$imgAvatar->getClientOriginalName();
	        $imgAvatar->move('uploads/images/news', $nameAvatar);
	        $arrInsert['ne_avatar'] = $nameAvatar;
        }
        $idNews = DB::table('news')->insertGetId($arrInsert);
        if($idNews)
        {
        	return redirect('mx-admin/quan-ly-bai-viet');
        }
        else
        {
        	return back()->with('error', 'Đăng bài không thành công');
        }
    }
    public function listNews()
    {
    	$data['total'] = DB::table('news')->where('ne_type', 0)->count();
    	$data['news']  = DB::table('news')
                        ->where('ne_type', 0)
                        ->leftJoin('category', 'ne_cate', 'cat_id')
                        ->leftJoin('users', 'ne_user_id', 'id')
                        ->paginate(20);

    	return view('BackEnd/Pages/News/list', $data);
    }
    public function editNews($news_id)
    {
        $data['cates'] = DB::table('category')->where('cat_parent_id', 0)->get();
        $data['news']  = DB::table('news')
                        ->where('ne_id', $news_id)
                        ->leftJoin('category', 'ne_cate', 'cat_id')
                        ->leftJoin('users', 'ne_user_id', 'id')
                        ->get()[0];
        return view('BackEnd/Pages/News/edit', $data);
    }
    public function postEditNews(Request $request, $news_id)
    {
        $title = trim($request->title);
        $slug  = str_slug($request->title);

        $isNews = DB::table('news')
                    ->whereNotIn('ne_id', [ $news_id ])
                    ->where('ne_slug', $slug)->get();
        if ( $isNews->count() > 0 ) {
            return back()->with('error', 'Tiêu đề này đã tồn tại');
        }

        $arrUpdate   = [];
        $arrUpdate['ne_title']      = $title;
        $arrUpdate['ne_slug']       = $slug;
        $arrUpdate['ne_des']        = trim($request->des);
        $arrUpdate['ne_content']    = trim($request->content);
        $arrUpdate['ne_tags']       = trim($request->tags);
        $arrUpdate['ne_keyword']    = trim($request->keyword);
        $arrUpdate['ne_description']= trim($request->description);

        if($imgAvatar = $request->file('avatar'))
        {
            $nameAvatar = rand().$imgAvatar->getClientOriginalName();
            $imgAvatar->move('uploads/images/news', $nameAvatar);
            $arrUpdate['ne_avatar'] = $nameAvatar;
        }
        $idNews = DB::table('news')->where('ne_id', $news_id)->update($arrUpdate);
        return redirect('mx-admin/xem-bai-viet/'.$news_id);
    }
    public function viewNews($news_id)
    {
        $data['news']  = DB::table('news')
                        ->where('ne_id', $news_id)
                        ->leftJoin('category', 'ne_cate', 'cat_id')
                        ->leftJoin('users', 'ne_user_id', 'id')
                        ->get()[0];
        return view('BackEnd/Pages/News/view', $data);
    }
    public function deleteNews($news_id)
    {
        $data['news']  = DB::table('news')
                        ->where('ne_id', $news_id)
                        ->delete();
        return redirect('mx-admin/quan-ly-bai-viet');
    }
}