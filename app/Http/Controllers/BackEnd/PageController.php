<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use DB;

class PageController extends Controller
{
    public function dashboard()
    {
    	return view('BackEnd/Pages/dashboard');
    }

    public function settingWebsite()
    {
        $data['st_info'] = DB::table('setting')->get()[0];
        return view('BackEnd/Pages/setting_website', $data);
    }
    public function postSettingWebsite(Request $request)
    {
        $title = trim($request->title);
        $box_footer = json_encode($request->box_footer);

        $arrInsert = [
            'st_title'      => $title,
            'st_company'    => trim($request->company),
            'st_address'    => trim($request->address),
            'st_phone'      => trim($request->phone),
            'st_phone2'      => trim($request->phone2),
            'st_nhamay'      => trim($request->nhamay),
            'st_email'      => trim($request->email),
            'st_facebook'   => trim($request->facebook),
            'st_google'     => trim($request->google)
        ];

        DB::table('setting')->delete();

        $idIn = DB::table('setting')->insertGetId($arrInsert);
        return back();
    }

    /**
     * [listUsers description] Danh sách user
     * @return [type] [description]
     */
    public function listUsers()
    {
    	$data['total']		= User::count();
    	$data['list_user'] 	= User::paginate(20);
    	return view('BackEnd/Pages/Users/list', $data);
    }

    /**
     * [banner description] Quản lý Banner
     * @return [type] [description]
     */
    public function banner()
    {
        $data['banner'] = DB::table('banner')->orderby('bn_id', 'DESC')->limit(6)->get();
        return view('BackEnd/Pages/Banner/setting', $data);
    }

    public function setBanner(Request $request)
    {
        $getID = $request->id_bn;
        if ($getID == null) {
            DB::table('banner')->delete();
        }
        elseif (count($getID) > 0) {
            DB::table('banner')->whereNotIn('bn_id', $getID)->delete();
        }
        if($files = $request->file('avatar'))
        {
            foreach($files as $file)
            {
                $name = rand().$file->getClientOriginalName();
                $file->move('uploads/images/banner',$name);
                DB::table('banner')->insert([ 'bn_image' => $name ]);
            }
        }
        return back();
    }

    /**
     * [banner description] Quản lý Banner
     * @return [type] [description]
     */
    public function video()
    {
        return view('BackEnd/Pages/Video/setting');
    }

    public function postVideo(Request $request)
    {   
        $ma_nhung = trim($request->ma_nhung_youtube);
        if ($ma_nhung == '')
        {
            return back()->with('error', 'Bạn chưa dán iframe');
        }
        DB::table('video')->insertGetId([
            'vd_src' => $ma_nhung
        ]);
        return redirect('mx-admin/video');
    }

    public function updateVideo(Request $request, $id_video)
    {
        $ma_nhung = trim($request->ma_nhung_youtube);
        if ($ma_nhung == '')
        {
            return back()->with('error', 'Bạn chưa dán iframe');
        }
        DB::table('video')->where('vd_id', $id_video)->update([
            'vd_src' => $ma_nhung
        ]);
        return redirect('mx-admin/video');
    }
    public function listVideo()
    {
        $data['video'] = DB::table('video')->paginate(4);
        return view('BackEnd/Pages/Video/list', $data);
    }
    public function deleteVideo($id_video)
    {
        DB::table('video')->where('vd_id', $id_video)->delete();
        return redirect('mx-admin/video');
    }
    
    /**
     * [logo description] Chỉnh sửa logo
     * @return [type] [description]
     */
    public function logo()
    {
        $data['picture'] = DB::table('logo')->get();
        return view('BackEnd/Pages/Logo/setting', $data);
    }
    public function postLogo(Request $request)
    {
        DB::table('logo')->delete();
        
        if($imgLogo = $request->file('img_logos'))
        {
            $name   = rand().$imgLogo->getClientOriginalName();
            $idLogo = DB::table('logo')->insertGetId([
                'lg_image' => $name
            ]);
            if ($idLogo) {
                $imgLogo->move('images',$name);
            }
        }
        return back();
    }

    public function widgetSetting()
    {
        $data['getCode'] = DB::table('more_setting')->get()[0];
        return view('BackEnd/Pages/Widget/setting', $data);
    }
    public function pWidgetSetting(Request $request)
    {
        $getCode = DB::table('more_setting');
        $select = $request->option_sl;
        if ($select == 0) {
            // Google Maps
            if ($getCode->count() < 1)
            {
                $getCode->insert([
                    'ms_maps' => $request->maps
                ]);
            }
            else
            {
                $getCode->update([
                    'ms_maps' => $request->maps
                ]);
            }
        }
        if ($select == 1) {
            // Facebook
            if ($getCode->count() < 1)
            {
                $getCode->insert([
                    'ms_face' => $request->plugin_fb
                ]);
            }
            else
            {
                $getCode->update([
                    'ms_face' => $request->plugin_fb
                ]);
            }
        }
        return back();
    }
    public function renderAbout()
    {
        $data['about'] = DB::table('page_about')->get();
        return view('BackEnd/Pages/Widget/lienhe', $data);
    }
    public function postRenderAbout(Request $request)
    {
        $content = $request->content;

        $tableP = DB::table('page_about');
        if($tableP->count() < 1)
        {
            $tableP->insert([ 'pa_content' => $content ]);
        }
        else
        {
            $tableP->update([ 'pa_content' => $content ]);
        }
        return back();
    }
    public function renderKhoahoc()
    {
        $data['khoahoc'] = DB::table('page_hd_khoahoc')->get();
        return view('BackEnd/Pages/Widget/khoahoc', $data);
    }
    public function postRenderKhoahoc(Request $request)
    {
        $content = $request->content;

        $tableP = DB::table('page_hd_khoahoc');
        if($tableP->count() < 1)
        {
            $tableP->insert([ 'kh_content' => $content ]);
        }
        else
        {
            $tableP->update([ 'kh_content' => $content ]);
        }
        return back();
    }
}
