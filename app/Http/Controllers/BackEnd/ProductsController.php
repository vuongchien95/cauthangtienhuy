<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB, Auth;

class ProductsController extends Controller
{
    public function addProduct()
    {
        $data['cates'] = DB::table('category')->get();
        return view('BackEnd/Pages/Products/add', $data);
    }
    public function postAddProduct(Request $request)
    {
        $title = trim($request->title);
        $slug  = str_slug($request->title);

        if ($title == '') {
           return back()->with('error', 'Tên sản phẩm không được để trống');
        }
        $isPro = DB::table('products')->where([ ['pro_slug', $slug],['pro_cate', $request->cate] ])->count();
        if ($isPro > 0) {
            return back()->with('error', 'Tên sản phẩm đã tồn tại');
        }
        
        $arrInsert = [];

        $arrInsert['pro_title']       = $title; 
        $arrInsert['pro_slug']        = $slug;
        $arrInsert['pro_keyword']     = $request->keyword;
        $arrInsert['pro_tags']        = $request->tags;
        $arrInsert['pro_description'] = $request->description;
        $arrInsert['pro_content']     = $request->chi_tiet;
        $arrInsert['pro_user_id']     = Auth::user()->id;
        $arrInsert['price']           = trim($request->price);
        $arrInsert['new_price']       = trim($request->new_price);
        $arrInsert['xuat_xu']         = trim($request->xuat_xu);
        $arrInsert['pro_cate']        = $request->cate;

        if($imgAvatar = $request->file('avatar'))
        {
            $nameAvatar = rand().$imgAvatar->getClientOriginalName();
            $imgAvatar->move('uploads/images/Products', $nameAvatar);
            $arrInsert['pro_avatar'] = $nameAvatar;
        }

        $idPro = DB::table('products')->insertGetId($arrInsert);
        if ($idPro) {
            
                DB::table('cate_product')->insert([
                    'cp_cate_id' => $request->cate,
                    'cp_prod_id' => $idPro
                ]);

            return redirect('mx-admin/san-pham');
        }
        else
        {
            return back()->with('error', 'Lỗi! Vui lòng kiểm tra lại');
        }
    }
    /**
     * [product description] List sản phẩm
     * @return [type] [description]
     */
    public function product()
    {
        $data['total'] = DB::table('products')->count();
        $data['products'] = DB::table('products')
                            ->orderby('pro_id', 'desc')
                            ->leftJoin('users', 'pro_user_id', 'id')
                            ->paginate(20);
        return view('BackEnd/Pages/Products/list', $data);
    }

    public function editProduct($pro_id)
    {
        $data['cates'] = DB::table('category')->get();
        $data['products'] = DB::table('products')
                            ->where('pro_id', $pro_id)
                            ->leftJoin('cate_product', 'pro_id', 'cp_prod_id')
                            ->leftJoin('users', 'pro_user_id', 'id')
                            ->get()[0];
        return view('BackEnd/Pages/Products/edit', $data);
    }

    public function postEditProduct(Request $request, $pro_id)
    {

        $title = trim($request->title);
        $slug  = str_slug($request->title);

        $isPro = DB::table('products')
                    ->whereNotIn('pro_id', [$pro_id])
                    ->where([ ['pro_slug', $slug],['pro_cate', $request->cate] ])->count();
        if ($isPro > 0) {
            return back()->with('error', 'Tên sản phẩm đã tồn tại');
        }

        // $arrCate = $request->cate;
        // if (count($arrCate) < 1) {
        //     return back()->with('error', 'Vui lòng chọn danh mục');
        // }
        $arrUpdate = [];

        $arrUpdate['pro_title']       = $title; 
        $arrUpdate['pro_slug']        = $slug;
        $arrUpdate['pro_keyword']     = $request->keyword;
        $arrUpdate['pro_tags']        = $request->tags;
        $arrUpdate['pro_description'] = $request->description;
        $arrUpdate['pro_content']     = $request->mo_ta;
        $arrUpdate['pro_user_id']     = Auth::user()->id;
        $arrInsert['duong_kinh']      = trim($request->duong_kinh);
        $arrInsert['kich_thuoc']      = trim($request->kich_thuoc);
        $arrInsert['bao_hanh']        = trim($request->bao_hanh);
        $arrUpdate['price']           = trim($request->price);
        $arrUpdate['new_price']       = trim($request->new_price);
        $arrUpdate['pro_cate']        = $request->cate;

        if($imgAvatar = $request->file('avatar'))
        {
            $nameAvatar = rand().$imgAvatar->getClientOriginalName();
            $imgAvatar->move('uploads/images/Products', $nameAvatar);
            $arrUpdate['pro_avatar'] = $nameAvatar;
        }

        DB::table('cate_product')->where('cp_prod_id', $pro_id)->delete();
        
            DB::table('cate_product')->insert([
                'cp_cate_id' => $request->cate,
                'cp_prod_id' => $pro_id
            ]);


        DB::table('products')->where('pro_id', $pro_id)->update($arrUpdate);
        return redirect('mx-admin/san-pham');
    }

    public function deleteProduct($pro_id)
    {
        DB::table('cate_product')->where('cp_prod_id', $pro_id)->delete();
        DB::table('products')->where('pro_id', $pro_id)->delete();
        return redirect('mx-admin/san-pham');
    }
}
