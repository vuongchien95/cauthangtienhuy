<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;

class UserController extends Controller
{
    public function addUser()
    {
    	return view('BackEnd/Pages/Users/add');
    }
    /**
     * [postAddUser description] Xử lý thêm thành viên
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postAddUser(Request $request)
    {
    	$name 		= trim($request->name);
    	$email 		= trim($request->email);
    	$password 	= trim($request->password);
    	$level 		= trim($request->level);

    	if(str_slug($name) == '' || str_slug($email) == '' || str_slug($password) == '')
    	{
    		return back()->with('error', 'Vui lòng điền đầy đủ thông tin');
    	}

    	if (strlen($name) < 3)
    	{
    		return back()->with('error', 'Tên phải nhiều hơn 3 ký tự');
    	}

    	if (strlen($password) < 3)
    	{
    		return back()->with('error', 'Mật khẩu phải nhiều hơn 3 ký tự');
    	}

    	$isEmail = User::where('email', $email)->count();
    	if ($isEmail > 0) {
    		return back()->with('error', 'Email đã được sử dụng, vui lòng nhập email khác');
    	}

    	$idUser = User::insertGetId([
    		'name' 		=> $name,
	    	'email' 	=> $email,
	    	'password' 	=> $password,
	    	'level' 	=> $level
    	]);
    	if ($idUser)
    	{
    		return redirect('mx-admin/quan-ly-thanh-vien');
    	}
    	else
    	{
    		return back()->with('error', 'Tạo mới không thành công vui lòng kiểm tra lại');
    	}
    }

    public function editUser($user_id)
    {
    	$data['user_info'] = User::where('id', $user_id)->get()[0];
    	return view('BackEnd/Pages/Users/edit', $data);
    }
    public function postEditUser(Request $request, $user_id)
    {
    	$isUser = User::where('id', $user_id)->count();

    	if ($isUser < 1) {
    		return back()->with('error', 'Không tồn tại thành viên này');
    	}

    	$name 		= trim($request->name);
    	$password 	= trim($request->password);
    	$email 		= trim($request->email);
    	$level		= $request->level;

    	if(str_slug($name) == '' || str_slug($email) == '')
    	{
    		return back()->with('error', 'Vui lòng điền đầy đủ thông tin');
    	}

    	if (strlen($name) < 3)
    	{
    		return back()->with('error', 'Tên phải nhiều hơn 3 ký tự');
    	}

    	if ($password != '' && strlen($password) < 3)
    	{
    		return back()->with('error', 'Mật khẩu phải nhiều hơn 3 ký tự');
    	}

    	$arrUpdate = [
    		'name' 	=> $name,
    		'email' => $email,
    		'level' => $level
    	];

    	if($password != '' && strlen($password) >= 3 ) {
    		$arrUpdate['password'] = $password;
    	}
    	User::where('id', $user_id)->update($arrUpdate);
    	return redirect('mx-admin/quan-ly-thanh-vien');
    }
    public function deleteUser($user_id)
    {
    	$isUser = User::where('id', $user_id)->count();
    	if ($isUser < 1) {
    		return back()->with('error', 'Không tồn tại thành viên này');
    	}
    	User::where('id', $user_id)->delete();
    	return redirect('mx-admin/quan-ly-thanh-vien');
    }
}
