<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CateController extends Controller
{
    public function byCate($cate_id, $cate_slug)
    {
    	$data['getAbout'] = DB::table('page_about')->get();
    	return view('FrontEnd/pages/cate', $data);
    }
}
