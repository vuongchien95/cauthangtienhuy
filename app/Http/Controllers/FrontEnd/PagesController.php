<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Mail;

class PagesController extends Controller
{
    public function home()
    {
        $data['banner']     = DB::table('banner')->get();
        $data['products']   = DB::table('products')->limit(12)->get();
    	return view('FrontEnd.pages.home',$data);
    }

    public function lienhe()
    {
        // $data['tienich_image'] = DB::table('tn_images')->get();
        return view('FrontEnd.pages.lien-he');
    }
    public function tintuc()
    {
        $data['tintuc'] = DB::table('news')->paginate(9);
        return view('FrontEnd.pages.tin-tuc',$data);
    }
    public function detailtintuc($id, $ne_slug)
    {
        $data['tintuc'] = DB::table('news')->where('ne_id',$id)->get()[0];
        $data['tintuccu'] = DB::table('news')->orderby('ne_id', 'DESC')->limit(3)->get();
        return view('FrontEnd.pages.chi-tiet-new',$data);
    }
    public function gioithieu()
    {
        $data['gioithieu'] = DB::table('page_about')->get()[0];
        return view('FrontEnd.pages.gioi-thieu',$data);
    }
    public function khoahoc()
    {
        $data['khoahoc'] = DB::table('page_hd_khoahoc')->get()[0];
        return view('FrontEnd.pages.khoa-hoc',$data);
    }
    public function shops()
    {
        $data['products'] = DB::table('products')->paginate(12);
        return view('FrontEnd.pages.shops',$data);
    }
    public function detailProduct($id,$pro_slug)
    {
        $data['products'] = DB::table('products')->where('pro_id',$id)->get()[0];
        return view('FrontEnd.pages.chi-tiet-sanpham',$data);
    }
    public function byCate($id_dm, $slug_dm)
    {
        $data['cates']    = DB::table('category')->where('cat_id', $id_dm)->get();
        $data['products'] = DB::table('products')->where('pro_cate', $id_dm)->paginate(12);
        return view('FrontEnd.pages.bycate',$data);
    }

}
