<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use DB;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $ma_nhung_header = DB::table('embed_code')->where('em_option', 1)->get();
        $ma_nhung_body   = DB::table('embed_code')->where('em_option', 2)->get();
        $ma_nhung_footer = DB::table('embed_code')->where('em_option', 3)->get();

        $tintuc = DB::table('news')->orderby('ne_id', 'DESC')->limit(3)->get();

        $setting = DB::table('setting')->get();
       
        $logo = DB::table('logo')->get();

        $video = DB::table('video')->get();

        $more_st = DB::table('more_setting')->get();

        $getMenu = DB::table('category')->get();

        View::share([
            'tintuc_left'        => $tintuc,
            'logo'          => $logo,
            'setting'       => $setting,
            'more_st'       => $more_st,
            'video'         => $video,
            'ma_nhung_header'   => $ma_nhung_header,
            'ma_nhung_body'     => $ma_nhung_body,
            'ma_nhung_footer'   => $ma_nhung_footer,
            'getMenu'           => $getMenu
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
