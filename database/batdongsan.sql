-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th4 17, 2018 lúc 08:47 AM
-- Phiên bản máy phục vụ: 10.1.26-MariaDB
-- Phiên bản PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `batdongsan`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `banner`
--

CREATE TABLE `banner` (
  `bn_id` int(11) NOT NULL,
  `bn_image` text COLLATE utf8_unicode_ci,
  `bn_created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `bn_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `banner`
--

INSERT INTO `banner` (`bn_id`, `bn_image`, `bn_created_at`, `bn_updated_at`) VALUES
(2, '843499736slide-home1.png', '2018-04-13 03:27:24', NULL),
(3, '315737539z961251316077_9a57659c1704b69951f1b53fd01f33c5.jpg', '2018-04-16 09:19:58', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category`
--

CREATE TABLE `category` (
  `cat_id` int(11) NOT NULL,
  `cat_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cat_slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cat_parent_id` int(11) NOT NULL,
  `cat_created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `cat_updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `category`
--

INSERT INTO `category` (`cat_id`, `cat_name`, `cat_slug`, `cat_parent_id`, `cat_created_at`, `cat_updated_at`) VALUES
(1, 'Bồn inox đứng', 'bon-inox-dung', 0, '2018-03-29 08:57:59', '2018-03-29 08:57:59'),
(2, 'Bồn inox ngang', 'bon-inox-ngang', 0, '2018-03-29 08:59:04', '2018-03-29 08:59:04'),
(3, 'Bồn nhựa đứng', 'bon-nhua-dung', 0, '2018-03-29 08:59:26', '2018-03-29 08:59:26'),
(4, 'Bồn nhựa ngang', 'bon-nhua-ngang', 0, '2018-03-29 08:59:36', '2018-03-29 08:59:36'),
(5, 'Chậu rửa inox', 'chau-rua-inox', 0, '2018-03-29 08:59:54', '2018-03-29 08:59:54');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `cate_product`
--

CREATE TABLE `cate_product` (
  `cp_cate_id` int(11) NOT NULL,
  `cp_prod_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `cate_product`
--

INSERT INTO `cate_product` (`cp_cate_id`, `cp_prod_id`) VALUES
(1, 2),
(5, 3),
(1, 4),
(2, 5),
(3, 6),
(1, 8),
(2, 9),
(2, 10),
(5, 11),
(5, 12),
(3, 13),
(4, 7),
(1, 2),
(5, 3),
(1, 4),
(2, 5),
(3, 6),
(1, 8),
(2, 9),
(2, 10),
(5, 11),
(5, 12),
(3, 13),
(4, 7);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `embed_code`
--

CREATE TABLE `embed_code` (
  `em_id` int(11) NOT NULL,
  `em_code` text COLLATE utf8_unicode_ci,
  `em_option` int(11) DEFAULT NULL,
  `em_type` int(11) DEFAULT NULL COMMENT '1 là css, 2 là js',
  `em_created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `em_updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `embed_code`
--

INSERT INTO `embed_code` (`em_id`, `em_code`, `em_option`, `em_type`, `em_created_at`, `em_updated_at`) VALUES
(1, '', 1, NULL, '2018-03-13 07:27:00', '2018-03-13 07:27:00'),
(2, '', 2, NULL, '2018-03-13 07:27:14', '2018-03-13 07:27:14'),
(3, '', 3, NULL, '2018-03-13 07:27:14', '2018-03-13 07:27:14');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `logo`
--

CREATE TABLE `logo` (
  `lg_id` int(11) NOT NULL,
  `lg_image` text COLLATE utf8_unicode_ci,
  `lg_created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `lg_updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `logo`
--

INSERT INTO `logo` (`lg_id`, `lg_image`, `lg_created_at`, `lg_updated_at`) VALUES
(1, '1874256129logo_new.png', '2018-04-16 09:43:47', '2018-04-16 09:43:47');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mb_images`
--

CREATE TABLE `mb_images` (
  `id` int(11) NOT NULL,
  `mb_image` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `mb_images`
--

INSERT INTO `mb_images` (`id`, `mb_image`, `created_at`, `updated_at`) VALUES
(6, '226165434donlap.jpg', '2018-04-17 06:24:50', '2018-04-17 06:24:50'),
(7, '198145163songlap.jpg', '2018-04-17 06:24:50', '2018-04-17 06:24:50'),
(8, '1791340394lienke.jpg', '2018-04-17 06:24:50', '2018-04-17 06:24:50'),
(9, '857165030lienke.jpg', '2018-04-17 06:24:50', '2018-04-17 06:24:50'),
(10, '261992099shop.jpg', '2018-04-17 06:24:50', '2018-04-17 06:24:50');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mb_images_pk`
--

CREATE TABLE `mb_images_pk` (
  `id` int(11) NOT NULL,
  `mb_image_pk` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `mb_images_pk`
--

INSERT INTO `mb_images_pk` (`id`, `mb_image_pk`, `created_at`, `updated_at`) VALUES
(1, '1992378486maudon-705x532.png', '2018-04-16 04:19:51', '2018-04-16 04:19:51'),
(2, '1069246130huongduong-705x532.png', '2018-04-16 04:19:51', '2018-04-16 04:19:51'),
(3, '1005586253phonglan-705x532.png', '2018-04-16 04:19:51', '2018-04-16 04:19:51'),
(4, '2023656876nguyetque-705x532.png', '2018-04-16 04:19:51', '2018-04-16 04:19:51'),
(5, '1053843883hoahong-705x532.png', '2018-04-16 04:19:51', '2018-04-16 04:19:51');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `more_setting`
--

CREATE TABLE `more_setting` (
  `ms_maps` text COLLATE utf8_unicode_ci,
  `ms_face` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `more_setting`
--

INSERT INTO `more_setting` (`ms_maps`, `ms_face`) VALUES
('<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1862.2882053307708!2d105.80375020814624!3d21.009610296502174!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135acbd5a8f5ce5%3A0x89160de46efae4e2!2zQ8O0bmcgVHkgVMOibiBN4bu5!5e0!3m2!1svi!2s!4v1522379999693\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', '<iframe src=\"https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fbonnuocinoxtanmy&tabs=timeline&width=340&height=500&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=503129500119109\" width=\"340\" height=\"200\" style=\"border:none;overflow:hidden\" scrolling=\"no\" frameborder=\"0\" allowTransparency=\"true\"></iframe>'),
('<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1862.2882053307708!2d105.80375020814624!3d21.009610296502174!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135acbd5a8f5ce5%3A0x89160de46efae4e2!2zQ8O0bmcgVHkgVMOibiBN4bu5!5e0!3m2!1svi!2s!4v1522379999693\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', '<iframe src=\"https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fbonnuocinoxtanmy&tabs=timeline&width=340&height=500&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=503129500119109\" width=\"340\" height=\"200\" style=\"border:none;overflow:hidden\" scrolling=\"no\" frameborder=\"0\" allowTransparency=\"true\"></iframe>');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `news`
--

CREATE TABLE `news` (
  `ne_id` int(11) NOT NULL,
  `ne_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ne_slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ne_cate` int(11) NOT NULL,
  `ne_des` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ne_content` text COLLATE utf8_unicode_ci NOT NULL,
  `ne_tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ne_keyword` text COLLATE utf8_unicode_ci,
  `ne_description` text COLLATE utf8_unicode_ci,
  `ne_avatar` text COLLATE utf8_unicode_ci,
  `ne_view` int(11) DEFAULT '0',
  `ne_user_id` int(11) NOT NULL,
  `ne_type` int(11) NOT NULL,
  `ne_created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `ne_updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `setting`
--

CREATE TABLE `setting` (
  `st_id` int(11) NOT NULL,
  `st_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `st_company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `st_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `st_nhamay` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `st_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `st_phone2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `st_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `st_facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `st_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `st_created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `st_updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `setting`
--

INSERT INTO `setting` (`st_id`, `st_title`, `st_company`, `st_address`, `st_nhamay`, `st_phone`, `st_phone2`, `st_email`, `st_facebook`, `st_google`, `st_created_at`, `st_updated_at`) VALUES
(12, 'Vinhomes Star City - Thanh Hoá', 'Vinhomes Star City - Thanh Hoá', 'Tầng 10, tòa nhà Viettel, Số 386 Đại lộ Lê Lợi, Phường Đông Hương, TP Thanh Hóa', 'KCN Thạch Thất - Quốc Oai, TP.Hà Nội', '094.933.8686', '0915 951 692', 'daovanhungk5@gmail.com', '', '', '2018-04-16 09:33:19', '2018-04-16 09:33:19');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tien_nghi`
--

CREATE TABLE `tien_nghi` (
  `id` int(11) NOT NULL,
  `list_mo_ta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tien_nghi`
--

INSERT INTO `tien_nghi` (`id`, `list_mo_ta`, `created_at`, `updated_at`) VALUES
(1, 'Trung tâm thương mại Vincom', '2018-04-13 08:46:42', '2018-04-13 08:46:42'),
(2, 'Hệ thống an ninh, phục vụ tiêu chuẩn 5 sao', '2018-04-13 08:47:05', '2018-04-13 08:47:05'),
(3, 'Bể bơi mái kính bốn mùa', '2018-04-13 08:47:22', '2018-04-13 08:47:22'),
(4, 'Hệ thống trường liên cấp Vinschool', '2018-04-13 08:47:30', '2018-04-13 08:47:30'),
(5, 'Sân chơi trẻ em', '2018-04-13 08:47:36', '2018-04-13 08:47:36'),
(6, 'Hệ thống sân thể thao: cầu lông, bóng rổ, tennis…', '2018-04-13 08:47:46', '2018-04-13 08:47:46'),
(7, 'Giàn hoa, tiểu cảnh, đường dạo, đường chạy bộ…', '2018-04-13 08:47:52', '2018-04-13 08:47:52'),
(8, 'Tiểu cảnh nước, đài phun nước…', '2018-04-13 08:48:00', '2018-04-13 08:48:00'),
(9, 'Vườn điêu khắc', '2018-04-13 08:48:08', '2018-04-13 08:48:08');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tn_images`
--

CREATE TABLE `tn_images` (
  `id` int(11) NOT NULL,
  `tn_image` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tn_images`
--

INSERT INTO `tn_images` (`id`, `tn_image`, `created_at`, `updated_at`) VALUES
(6, '339388739tien-ich-vinhomes-star-city-800x430.png', '2018-04-13 08:15:16', '2018-04-13 08:15:16'),
(7, '1313677406slide2-1-650x430.png', '2018-04-13 08:22:08', '2018-04-13 08:22:08'),
(8, '1070176019slide3-1-650x430.png', '2018-04-13 08:22:09', '2018-04-13 08:22:09'),
(9, '735815290slide4-1-650x430.png', '2018-04-13 08:22:09', '2018-04-13 08:22:09'),
(10, '1257861160slide6-1-650x430.png', '2018-04-13 08:22:09', '2018-04-13 08:22:09'),
(11, '1514071489slide7-1-650x430.png', '2018-04-13 08:22:09', '2018-04-13 08:22:09');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tongquan`
--

CREATE TABLE `tongquan` (
  `id` int(11) NOT NULL,
  `tenduan` varchar(255) DEFAULT NULL,
  `dautu` varchar(255) DEFAULT NULL,
  `dientich` varchar(255) DEFAULT NULL,
  `vitri` varchar(255) DEFAULT NULL,
  `quanly` varchar(255) DEFAULT NULL,
  `sohuu` varchar(255) DEFAULT NULL,
  `socan` varchar(255) DEFAULT NULL,
  `sanpham` varchar(255) DEFAULT NULL,
  `moban` varchar(255) DEFAULT NULL,
  `tienich` varchar(255) DEFAULT NULL,
  `khuchinh` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `tongquan`
--

INSERT INTO `tongquan` (`id`, `tenduan`, `dautu`, `dientich`, `vitri`, `quanly`, `sohuu`, `socan`, `sanpham`, `moban`, `tienich`, `khuchinh`, `created_at`, `updated_at`) VALUES
(20, 'Vinhomes Star City', 'Tập đoàn VinGroup', '118,5 ha', 'Đại lộ Lê Lợi – Phường Đông Hải – Đông Hương – Tp. Thanh Hóa', 'Vinhomes', 'Sổ đỏ vĩnh viễn', '960 căn', 'Biệt thự đơn lập, biệt thự song lập, nhà liền kề, nhà vườn, nhà phố thương mại Shophouse', 'Phân khu Hoa Hồng 267 căn', 'Khu thể thao ngoài trời, bể bơi mái kính, sân chơi trẻ em, sân thể thao, giàn hoa và đường dạo, tiểu cảnh nước, vườn điêu khắc…', 'Hoa Mẫu Đơn, Hoa Hồng, Phong Lan, Nguyệt Quế, Hướng Dương', '2018-04-13 06:56:05', '2018-04-13 06:56:05');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tq_images`
--

CREATE TABLE `tq_images` (
  `id` int(11) NOT NULL,
  `tq_image` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `tq_images`
--

INSERT INTO `tq_images` (`id`, `tq_image`, `created_at`, `updated_at`) VALUES
(12, '1648116585z961297061312_ab78f0ef61721adcc94bd48bb41a7a9d.jpg', '2018-04-17 06:20:13', '2018-04-17 06:20:13'),
(13, '52046397z961297121826_9b7c87a2be75bc115f211836a7b2a629.jpg', '2018-04-17 06:20:13', '2018-04-17 06:20:13'),
(14, '1151403774z961297161331_9ff273dcb70807f2bcda70b711e3f0b4.jpg', '2018-04-17 06:20:13', '2018-04-17 06:20:13'),
(15, '1719326426z961297207973_81fba8d725bdf181dcc106af65f3854f.jpg', '2018-04-17 06:20:13', '2018-04-17 06:20:13'),
(16, '1474320497z961297253900_feafdc3c5c116be492fb969d322e51bc.jpg', '2018-04-17 06:20:14', '2018-04-17 06:20:14'),
(17, '1609257887z961297400909_6d09583a9272d815efcba033facf49db.jpg', '2018-04-17 06:20:14', '2018-04-17 06:20:14'),
(18, '1604147835z961297380930_45da70c73100237a36b9963f41fe24fd.jpg', '2018-04-17 06:20:14', '2018-04-17 06:20:14'),
(19, '864321709z961297364190_beb5965590407e06f67372b4e47f8835.jpg', '2018-04-17 06:20:14', '2018-04-17 06:20:14'),
(20, '1535289692z961297323821_76e2592523ccf60ee3c2fb4e5378c299.jpg', '2018-04-17 06:20:14', '2018-04-17 06:20:14'),
(21, '1681507130z961297301641_21f25df35e62237d6f68b3a39d5146ec.jpg', '2018-04-17 06:20:14', '2018-04-17 06:20:14');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `level`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', '$2y$10$Npn8v5aMpLhpuuLXgrJiA.d3i.GxToH48gIffylGl2uVCeiQxGtDK', '0', 'NEgYQqqlXm6Nng2ilpmDDsEfCniD2Yrn9PIr3lW5Yd8r3pHQvtuWlPNoc1tZ', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `video`
--

CREATE TABLE `video` (
  `vd_id` int(11) NOT NULL,
  `vd_src` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `video`
--

INSERT INTO `video` (`vd_id`, `vd_src`) VALUES
(2, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/Fos8jUcoqI8\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `vitri`
--

CREATE TABLE `vitri` (
  `id` int(11) NOT NULL,
  `vitri` text NOT NULL,
  `img` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`bn_id`);

--
-- Chỉ mục cho bảng `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Chỉ mục cho bảng `embed_code`
--
ALTER TABLE `embed_code`
  ADD PRIMARY KEY (`em_id`);

--
-- Chỉ mục cho bảng `logo`
--
ALTER TABLE `logo`
  ADD PRIMARY KEY (`lg_id`);

--
-- Chỉ mục cho bảng `mb_images`
--
ALTER TABLE `mb_images`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `mb_images_pk`
--
ALTER TABLE `mb_images_pk`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`ne_id`);

--
-- Chỉ mục cho bảng `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`st_id`);

--
-- Chỉ mục cho bảng `tien_nghi`
--
ALTER TABLE `tien_nghi`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tn_images`
--
ALTER TABLE `tn_images`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tongquan`
--
ALTER TABLE `tongquan`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tq_images`
--
ALTER TABLE `tq_images`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`vd_id`);

--
-- Chỉ mục cho bảng `vitri`
--
ALTER TABLE `vitri`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `banner`
--
ALTER TABLE `banner`
  MODIFY `bn_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `category`
--
ALTER TABLE `category`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `embed_code`
--
ALTER TABLE `embed_code`
  MODIFY `em_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `logo`
--
ALTER TABLE `logo`
  MODIFY `lg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `mb_images`
--
ALTER TABLE `mb_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `mb_images_pk`
--
ALTER TABLE `mb_images_pk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `news`
--
ALTER TABLE `news`
  MODIFY `ne_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `setting`
--
ALTER TABLE `setting`
  MODIFY `st_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `tongquan`
--
ALTER TABLE `tongquan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT cho bảng `tq_images`
--
ALTER TABLE `tq_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `video`
--
ALTER TABLE `video`
  MODIFY `vd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `vitri`
--
ALTER TABLE `vitri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
