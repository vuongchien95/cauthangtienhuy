@extends('BackEnd.LayOut.master')
@section('title', 'Thêm danh mục mới - Hệ thống quản trị website')
@section('main-content')

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
            <a href="{{ url('mx-admin/danh-muc-san-pham') }}">
                Danh mục sản phẩm
            </a>
            <a href="">
                Thêm mới
            </a>
        </div>
        <h1 class="ttl-add-use" style="font-size: 25px"> Thêm danh mục </h1>
    </div>
    <!--End-breadcrumbs-->
    <!--Action boxes-->
    <div class="container-fluid">

        @include('Notify.note')

        <div class="row-fluid">
            <div class="widget-box" style="max-width: 600px;">
                <div class="widget-title"> <span class="icon"><i class="icon-ok"></i></span>
                    <h5> Danh mục sản phẩm </h5>
                </div>
                <div class="widget-content">
                    <div class="widget-content nopadding">
                        <form method="post" class="form-horizontal" style="max-width: 800px;">
                            {{ csrf_field() }}
                            <div class="control-group">
                                <div class="form-group">
                                    <label class="control-label">Tên danh mục: </label>
                                    <div class="controls">
                                        <input type="text" class="span11" name="name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Danh mục cấp cha: </label>
                                    <div class="controls">
                                        <select class="form-control" name="danh_muc_cap_cha">
                                            <option value="0"> Danh mục cấp 1 </option>
                                            @php
                                                selectCates($cates);
                                            @endphp
                                        </select>
                                    </div>
                                </div>
                                <div class="controls">
                                    <button type="submit" class="btn btn-success"> Tạo mới </button> 
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection