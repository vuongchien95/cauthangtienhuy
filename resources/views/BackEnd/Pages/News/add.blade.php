@extends('BackEnd.LayOut.master')
@section('title', 'Viết bài mới - Hệ thống quản trị website')
@section('main-content')

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
            <a href="">
                Tin tức
            </a>
            <a href="">
                Viết bài mới
            </a>
        </div>
    </div>
    <style type="text/css">
        .form-horizontal .controls { margin-left: 10px }
        input[type=text] { min-width: 50% }
    </style>
    <!--End-breadcrumbs-->
    <!--Action boxes-->
    <div class="container-fluid">

        @include('Notify.note')

        <div class="row-fluid">
            <div class="widget-box">
                <div class="widget-title"> <span class="icon"><i class="icon-ok"></i></span>
                    <h5> Viết bài mới </h5>
                </div>
                <div class="widget-content">
                    <div class="widget-content nopadding">
                        <form class="form-horizontal form-material" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="control-group">
                            <label class="controls">Tiêu đề <span style="color: red">(*)</span></label>
                            <div class="controls">
                                <input type="text" name="title" class="form-control form-control-line" style="padding-left: 10px" required>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="example-email" class="controls">Mô tả ngắn <span style="color: red">(*)</span></label>
                            <div class="controls">
                                <textarea name="des" class="form-control form-control-line" style="width: 98.7%" required></textarea>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="example-email" class="controls">Nội dung <span style="color: red">(*)</span></label>
                            <div class="controls">
                                <textarea name="content" id="content" class="ckeditor" cols="30" rows="10" required></textarea>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="controls">Tags</label>
                            <div class="controls">
                                <input type="text" name="tags" class="form-control form-control-line" autocomplete="off" style="padding-left: 10px">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="controls">Meta keyword</label>
                            <div class="controls">
                                <input type="text" name="keyword" class="form-control form-control-line" autocomplete="off" style="padding-left: 10px">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="controls">Meta description</label>
                            <div class="controls">
                                <input type="text" name="description" class="form-control form-control-line" autocomplete="off" style="padding-left: 10px">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="controls">Ảnh đại diện <span style="color: red">(*)</span></label>
                            <div class="controls">
                                <img src="{{ asset('images/no_images.png') }}" style="max-width: 195px; display: block">
                                <input class="inp-upload-img hide" type="file" name="avatar" onchange="changeImg(this)">
                                <span class="btn btn-sm btn-primary" onclick="$('.inp-upload-img').click()" style="display: block; max-width: 110px; margin: 20px 0"> Upload Image </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="col-sm-12">
                                <button class="btn btn-success">Đăng bài</button>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
@section('script')
    <script src="{{ asset('ckeditor/ckeditor.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        function changeImg(input){
            if(input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function(e){
                    $(input).prev().attr('src',e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection