@extends('BackEnd.LayOut.master')
@section('title', 'Quản lý bài viết - Hệ thống quản trị website')
@section('main-content')

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
            <a href="">
               Tin tức
            </a>
        </div>
    </div>
    <style type="text/css">
        th, td {
            border-left: 1px solid #ccc!important;
            border-top: 1px solid #ccc!important;
        }
        table {
            border-bottom: 1px solid #ccc!important;
            border-right: 1px solid #ccc!important;
        }
    </style>

    <div class="container-fluid">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-block">
                    <h4 class="card-title"> Danh Sách Bài Viết </h4>
                    <h6 class="card-subtitle">Tổng: <code>{{ $total }}</code></h6>
                    <div>
                        <a href="{{ url('mx-admin/viet-bai-moi') }}" class="btn btn-sm btn-success use-btn"><i class="mdi mdi-plus"></i> Viết bài mới </a>
                    </div>
                    <div class="table-responsive" style="padding-top: 20px">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Tiêu đề</th>
                                    <th class="hidden-xs">Tác giả</th>
                                    <th class="hidden-xs">Chuyên mục</th>
                                    <th class="hidden-xs">Tags</th>
                                    <th class="hidden-xs">Ngày đăng</th>
                                    <th style="width: 14.4%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($news as $nw)
                                <tr>
                                    <td>{{ $nw->ne_id }}</td>
                                    <td>{{ $nw->ne_title }}</td>
                                    <td class="hidden-xs">{{ $nw->name }}</td>
                                    <td class="hidden-xs">{{ $nw->cat_name }}</td>
                                    <td class="hidden-xs">{{ $nw->ne_tags }}</td>
                                    <td class="hidden-xs">{{ $nw->ne_created_at }}</td>
                                    <td>
                                        <a href="{{ url('mx-admin/xem-bai-viet/'.$nw->ne_id) }}" class="btn btn-sm btn-info use-btn"><i class="mdi mdi-eye-outline"></i> Xem </a>

                                        @if( Auth::user()->level < 2 || Auth::user()->id == $nw->ne_user_id )
                                            <a href="{{ url('mx-admin/sua-bai-viet/'.$nw->ne_id) }}" class="btn btn-sm btn-info use-btn"><i class="mdi mdi-border-color"></i> Sửa </a>
                                        @endif
                                        @if( Auth::user()->level == 0 )
                                            <a href="{{ url('mx-admin/xoa-bai-viet/'.$nw->ne_id) }}" class="btn btn-sm btn-danger use-btn"><i class="mdi mdi-delete-empty"></i> Xóa </a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $news->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop