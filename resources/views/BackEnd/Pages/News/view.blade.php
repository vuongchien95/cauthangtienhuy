@extends('BackEnd.LayOut.master')
@section('title', 'Quản lý tin tức - Hệ thống quản trị website')
@section('main-content')

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
            <a href="">
                Tin tức
            </a>
            <a href="">
                Chi tiết
            </a>
        </div>
    </div>
    <style type="text/css">
        label {
            font-weight: 600!important;
        }
        .form-group {
            border: 1px solid #ccc;
            padding: 14px;
            margin: 10px;
        }
        .ttl-add-use {
            font-size: 25px;
            text-align: left;
            margin: 20px 10px;
        }
    </style>

    <div class="container-fluid">
        <div class="col-md-12">
            <div class="card">
                <div class="card-block" style="background: none repeat scroll 0 0 #F9F9F9; padding-top: 10px; margin-top: 20px">
                    <div class="text-header text-center">
                        <h1 class="ttl-add-use"> {{ $news->ne_title }} </h1>
                        @if(Session::has('error'))
                            <div style="color: #f33; margin-left: 10px; margin-bottom: 20px">{{ Session::get('error') }}</div>
                        @endif
                    </div>
                    <form class="form-horizontal form-material" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="col-lg-6 col-md-6 box-left">
                            <div class="form-group">
                                <label class="col-md-12">Ảnh đại diện:</label>
                                <div class="col-md-12">
                                    @if($news->ne_avatar != '' && file_exists('uploads/images/news/'.$news->ne_avatar ))
                                        <img src="{{ asset('uploads/images/news/'.$news->ne_avatar) }}" style="max-width: 195px; display: block">
                                    @else
                                        <img src="{{ asset('images/no-image.png') }}" style="max-width: 195px; display: block">
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Mô tả ngắn:</label>
                                <div class="col-md-12 cnt-details">
                                   <p>{!! $news->ne_des !!}</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Nội dung:</label>
                                <div class="col-md-12 cnt-details">
                                   <p>{!! $news->ne_content !!}</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Tags:</label>
                                <div class="col-md-12">
                                    <p>{{ $news->ne_tags }}</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Meta keyword:</label>
                                <div class="col-md-12">
                                    <p>{{ $news->ne_keyword }}</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Meta description:</label>
                                <div class="col-md-12">
                                    <p>{{ $news->ne_description }}</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Chuyên mục:</label>
                                <div class="col-md-12">
                                    <p>{{ $news->cat_name}}</p>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-6 col-lg-6 box-left">
                            <div class="form-group">
                                <label class="col-md-12">Tác giả:</label>
                                <div class="col-md-12">
                                    <p>{{ $news->name}}</p>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-12">Ngày đăng:</label>
                                <div class="col-md-12">
                                    <p>{{ $news->ne_created_at}}</p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12">Ngày cập nhật:</label>
                                <div class="col-md-12">
                                    <p>{{ $news->ne_updated_at}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-lg-12 col-md-12" style="clear: both; border: none">
                            <div class="col-sm-12">
                                <a href="{{ url('mx-admin/sua-bai-viet/'.$news->ne_id) }}" class="btn btn-success">Sửa</a>
                                <a href="{{ url('mx-admin/xoa-bai-viet/'.$news->ne_id) }}" class="btn btn-warning">Xóa</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection