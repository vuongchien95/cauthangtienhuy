@extends('BackEnd.LayOut.master')
@section('title', 'Đăng sản phẩm mới - Hệ thống quản trị website')
@section('main-content')

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
            <a href="{{ url('mx-admin/san-pham') }}">
                Sản phẩm
            </a>
            <a href="">
                Đăng mới
            </a>
        </div>
    </div>
    <!--End-breadcrumbs-->
    <!--Action boxes-->
    <div class="container-fluid">

        @include('Notify.note')

        <div class="row-fluid">
            <div class="widget-box">
                <div class="widget-title"> <span class="icon"><i class="icon-ok"></i></span>
                    <h5> Đăng mới sản phẩm </h5>
                </div>
                <div class="widget-content">
                    <div class="widget-content nopadding">
                        <form  class="form-horizontal form-material" method="post" enctype="multipart/form-data" style="max-width: 800px;">
                            {{ csrf_field() }}
                            <div class="control-group">
                                <label class="control-label">Tiêu đề:</label>
                                <div class="controls">
                                    <input type="text" class="span11" name="title" required>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Ảnh đại diện:</label>
                                <div class="controls">
                                    <img src="{{ asset('images/no_images.png') }}" style="max-width: 195px; display: block">
                                    <input class="inp-upload-img hide" type="file" name="avatar" onchange="changeImg(this)">
                                    <span class="btn btn-sm btn-primary" onclick="$('.inp-upload-img').click()" style="display: block; max-width: 110px; margin: 20px 0"> Upload Image </span>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label"> Danh mục: </label>
                                <div class="controls">
                                    <select class="form-control form-control-line" name="cate">
                                        @foreach($cates as $ct)
                                            <option value="{{ $ct->cat_id }}">{{ $ct->cat_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Giá cũ:</label>
                                <div class="controls">
                                    <input type="text" name="price" class="span11 form-control form-control-line" autocomplete="off" style="padding-left: 10px">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Giá mới:</label>
                                <div class="controls">
                                    <input type="text" name="new_price" class="span11 form-control form-control-line" autocomplete="off" style="padding-left: 10px">
                                </div>
                            </div>

                            {{-- <div class="control-group">
                                <label class="control-label">Xuất xứ: </label>
                                <div class="controls">
                                    <input type="text" class="span11" name="xuat_xu">
                                </div>
                            </div>
                            

                            <div class="control-group">
                                <label for="control-label">Chi tiết sản phẩm:</label>
                                <div class="controls">
                                   <textarea name="chi_tiet" id="content" class="ckeditor" cols="30" rows="10" required></textarea>
                                </div>
                            </div> --}}

                            <div class="control-group">
                                <label class="control-label">Tags:</label>
                                <div class="controls">
                                    <input type="text" name="tags" class="span11 form-control form-control-line" autocomplete="off" style="padding-left: 10px">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Meta keyword:</label>
                                <div class="controls">
                                    <input type="text" name="keyword" class="span11 form-control form-control-line" autocomplete="off" style="padding-left: 10px">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Meta description:</label>
                                <div class="controls">
                                    <input type="text" name="description" class="span11 form-control form-control-line" autocomplete="off" style="padding-left: 10px">
                                </div>
                            </div>
                            
                            <div class="controls">
                                <button type="submit" class="btn btn-success"> Đăng </button> 
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
@section('script')
    <script src="{{ asset('ckeditor/ckeditor.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        function changeImg(input){
            if(input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function(e){
                    $(input).prev().attr('src',e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection