@extends('BackEnd.LayOut.master')
@section('title', 'Quản lý sản phẩm - Hệ thống quản trị website')
@section('main-content')

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
            <a href="">
                Sản phẩm
            </a>
        </div>
    </div>

    <div class="container-fluid">

        @include('Notify.note')

         <div class="widget-box">
            <div class="widget-title">
                <span class="icon"><i class="icon-ok"></i></span>
                <h5> Danh sách sản phẩm </h5>
            </div>
            <div class="widget-content nopadding">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Tên</th>
                            <th class="hidden-xs">Ảnh đại diện</th>
                            <th>Người đăng</th>
                            <th class="hidden-xs">Danh mục</th>
                            <th class="hidden-xs">Ngày đăng</th>
                            <th style="width: 120px">Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($products as $prod)
                            <tr>
                                <td style="text-align: center;">{{ $prod->pro_id }}</td>
                                <td style="text-align: center;">{{ $prod->pro_title }}</td>
                                <td class="hidden-xs" style="text-align: center;">
                                    <img src="{{ asset('uploads/images/Products/'.$prod->pro_avatar) }}" style="max-width: 200px;">
                                </td>
                                <td style="text-align: center;">{{ $prod->name }}</td>
                                <td class="hidden-xs" style="text-align: center;">
                                    @php
                                        $getCate = DB::table('category')->where('cat_id', $prod->pro_cate)->get();
                                        foreach ($getCate as $value) {
                                            echo "<span class='label label-info'>".$value->cat_name."</span>";
                                        }
                                    @endphp
                                </td>
                                <td class="hidden-xs" style="text-align: center;">{{ $prod->pro_created_at }}</td>
                                <td style="text-align: center;">
                                    <a class="tip" href="{{ url('mx-admin/sua-san-pham/'.$prod->pro_id) }}" data-original-title="Sửa"><i class="icon-pencil"></i> Sửa </a>
                                    &nbsp;
                                    <a class="tip" href="{{ url('mx-admin/xoa-san-pham/'.$prod->pro_id) }}" data-original-title="Xóa"><i class="icon-remove"></i> Xóa </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection
