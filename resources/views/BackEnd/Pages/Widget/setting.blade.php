@extends('BackEnd.LayOut.master')
@section('title', 'Phần mở rộng - Hệ thống quản trị website')
@section('main-content')

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
            <a href="">
                Phần mở rộng
            </a>
        </div>
        <h1 class="ttl-add-use" style="font-size: 25px"> Cài đặt phần mở rộng </h1>
    </div>
    <!--End-breadcrumbs-->
    <!--Action boxes-->
    <div class="container-fluid">

        @include('Notify.note')

        <div class="row-fluid">
            <div class="">
                <style type="text/css">
                    .item-video {
                        padding: 10px;
                        border: 1px solid #ddd;
                        margin-bottom: 20px;
                        background: #fff;
                    }
                    .item-video iframe { width: 100%!important } 
                </style>
               
                <div class="span5 item-video">
                    <form class="form-horizontal form-material" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="option_sl" value="0">
                        <div class="ttl-header">
                            <h5>Google Maps</h5>
                        </div>
                        <div class="text-iframe">
                            <textarea name="maps" style="width: 94.8%; min-height: 115px;">{{ $getCode->ms_maps }}</textarea>
                        </div>
                        <div class="btn-update">
                            <button class="btn btn-sm btn-warning pull-left"> Cập Nhật </button>
                        </div>
                    </form>
                    <div style="clear: both;"></div>
                </div>
                <div class="span5 item-video">
                    <form class="form-horizontal form-material" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                         <input type="hidden" name="option_sl" value="1">
                        <div class="ttl-header">
                            <h5>Plugin Xã Hội Facebook</h5>
                        </div>
                        <div class="text-iframe">
                            <textarea name="plugin_fb" style="width: 94.8%; min-height: 115px;">{{ $getCode->ms_face }}</textarea>
                        </div>
                        <div class="btn-update">
                            <button class="btn btn-sm btn-warning pull-left"> Cập Nhật </button>
                        </div>
                    </form>
                    <div style="clear: both;"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection