<div class="col-sm-18 col-md-18 padding_left">
    <div class="news_column ">
        <div class="panel-body">
            <h1 class="title margin-bottom-lg">{{ $tintuc->ne_title }}</h1>
            <div class="row margin-bottom-lg">
                <div class="col-md-12">
                   <em class="fa fa-clock-o">&nbsp;</em> <span class="h5">{{ $tintuc->ne_created_at }}</span>
                </div>
            </div>
            <div class="clearfix">
                <div class="hometext m-bottom"></div>
                <figure class="article center">
                    <img alt="{{ $tintuc->ne_title }}" src="{{ asset('uploads/images/news/'.$tintuc->ne_avatar) }}" width="460" class="img-thumbnail">
                </figure>
            </div>
            <div id="news-bodyhtml" class="bodytext margin-bottom-lg">
               {!! $tintuc->ne_content !!}
            </div>
        </div>
    </div>

    <div class="news_column ">
        <div class="panel-body">
            <div class="socialicon clearfix margin-bottom-lg">
                
            </div>
        </div>
    </div>
    <div class="news_column ">
        <div class="panel-body other-news">
            <p class="h3"><strong>Những tin cũ hơn</strong></p>
            <div class="clearfix">
                <ul class="related">
                    @foreach ($tintuccu as $news)
                        <li>
                        <em class="fa fa-angle-right">&nbsp;</em>
                        <a class="list-inline" href="{{ url('tin-tuc/'.$news->ne_id.'-'.$news->ne_slug) }}.html" data-placement="bottom" data-content="{{ $news->ne_title }}" data-rel="tooltip" data-original-title="" title="">{{ $news->ne_title }}</a>
                        <em>({{ $news->ne_created_at }})</em>
                    </li>
                    @endforeach
                    
                </ul>
            </div>
        </div>
    </div>
</div>