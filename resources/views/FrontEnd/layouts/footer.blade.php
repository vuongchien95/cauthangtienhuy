<!-- Start Footer bottom Area -->
<footer>
    <div class="footer-area">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="footer-content">
                        <div class="footer-head">
                            <h4>information</h4>
                            <hr>
                            <p>
                                You can contact us our consectetur adipisicing elit.
                            </p>
                            <div class="footer-contacts">
                                <p><span>Tel:</span> +4321-7654543</p>
                                <p><span>Email:</span> info@construction.com</p>
                                <p><span>Location:</span> House-50/2 Manhatan</p>
                            </div>
                            <div class="footer-icons">
                                <ul>
                                    <li>
                                        <a href="#">
                        <i class="fa fa-facebook"></i>
                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                        <i class="fa fa-twitter"></i>
                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                        <i class="fa fa-google"></i>
                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                        <i class="fa fa-pinterest"></i>
                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                        <i class="fa fa-instagram"></i>
                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end single footer -->
                <div class="col-md-3 hidden-sm col-xs-12">
                    <div class="footer-content">
                        <div class="footer-head">
                            <h4>Services Link</h4>
                            <hr>
                            <ul class="footer-list">
                                <li><a href="#">General Contracting</a></li>
                                <li><a href="#">Civil Works </a></li>
                                <li><a href="#">Architectural Design</a></li>
                                <li><a href="#">Infrastructure Works</a></li>
                                <li><a href="#">Electrical Systems</a></li>
                                <li><a href="#">Resconstruction Services</a></li>
                                <li><a href="#">Maintenance Services</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- end single footer -->
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="footer-content">
                        <div class="footer-head">
                            <h4>Recent Post</h4>
                            <hr>
                            <div class="single-blog">
                                <div class="blog-content">
                                    <div class="blog-title">
                                        <a href="#">
                                            <h4>Residential home decoration</h4>
                                        </a>
                                    </div>
                                    <div class="blog-text">
                                        <p>Redug Lagre dolor sit amet, consectetur adipisicing elit..</p>
                                        <a class="blog-btn" href="#">Read more</a>
                                    </div>
                                </div>
                            </div>
                            <div class="single-blog">
                                <div class="blog-content">
                                    <div class="blog-title">
                                        <a href="#">
                                            <h4>Dream house painting</h4>
                                        </a>
                                    </div>
                                    <div class="blog-text">
                                        <p>Redug Lagre dolor sit amet, consectetur adipisicing elit..</p>
                                        <a class="blog-btn" href="#">Read more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end single footer -->
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="footer-content">
                        <div class="footer-head">
                            <h4>Subscribe</h4>
                            <hr>
                            <p>
                                Are you looking for professional Construction services for your new home. Are you looking for professional Construction services for your dream house.
                            </p>
                            <div class="subs-feilds">
                                <div class="suscribe-input">
                                    <input type="email" class="email form-control width-80" id="sus_email" placeholder="Type Email">
                                    <button type="submit" id="sus_submit" class="add-btn">Subscribe</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end single footer -->
            </div>
        </div>
    </div>
    <div class="footer-area-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="copyright">
                        <p>
                            Copyright © 2018
                            <a href="http://maxad.vn">Maxad</a> All Rights Reserved
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="copyright">
                        <ul>
                            <li><a href="#">Giới thiệu</a></li>
                            <li><a href="#">Terms</a></li>
                            <li><a href="#">Private</a></li>
                            <li><a href="#">Policy</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>