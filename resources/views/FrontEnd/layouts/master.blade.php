<!DOCTYPE html>
<html lang="vi">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>@yield('title')</title>
        <meta name="description" content="@yield('description')">
        <meta name="keywords" content="@yield('keywords')">
        <!-- favicon -->    
        <link rel="shortcut icon" type="image/x-icon" href="img/logo/favicon.ico">
        <!-- all css here -->
        <!-- bootstrap v3.3.6 css -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
        <!-- owl.carousel css -->
        <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
        <link rel="stylesheet" href="{{ asset('css/owl.transitions.css') }}">
        <!-- meanmenu css -->
        <link rel="stylesheet" href="{{ asset('css/meanmenu.min.css') }}">
        <!-- font-awesome css -->
        <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/flaticon.css') }}">
        <link rel="stylesheet" href="{{ asset('css/icon.css') }}">
        <!-- magnific css -->
        <link rel="stylesheet" href="{{ asset('css/magnific.min.css') }}">
        <!-- venobox css -->
        <link rel="stylesheet" href="{{ asset('css/venobox.css') }}">
        <!-- style css -->
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        <!-- responsive css -->
        <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
        <!-- modernizr css -->
        <script src="{{ asset('js/vendor/modernizr-2.8.3.min.js') }}"></script>
    </head>
<body>
        {{-- header --}}
        @include('FrontEnd.layouts.header')

        @yield('content')
              

        {{-- footer --}}
        @include('FrontEnd.layouts.footer')
    
    <!-- all js here -->
    <!-- jquery latest version -->
    <script src="{{ asset('js/vendor/jquery-1.12.4.min.js') }}"></script>
    <!-- bootstrap js -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- owl.carousel js -->
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <!-- Counter js -->
    <script src="{{ asset('js/jquery.counterup.min.js') }}"></script>
    <!-- waypoint js -->
    <script src="{{ asset('js/waypoints.js') }}"></script>
    <!-- isotope js -->
    <script src="{{ asset('js/isotope.pkgd.min.js') }}"></script>
    <!-- stellar js -->
    <script src="{{ asset('js/jquery.stellar.min.js') }}"></script>
    <!-- magnific js -->
    <script src="{{ asset('js/magnific.min.js') }}"></script>
    <!-- venobox js -->
    <script src="{{ asset('js/venobox.min.js') }}"></script>
    <!-- meanmenu js -->
    <script src="{{ asset('js/jquery.meanmenu.js') }}"></script>
    <!-- Form validator js -->
    <script src="{{ asset('js/form-validator.min.js') }}"></script>
    <!-- plugins js -->
    <script src="{{ asset('js/plugins.js') }}"></script>
    <!-- main js -->
    <script src="{{ asset('js/main.js') }}"></script>
</body>

</html>
