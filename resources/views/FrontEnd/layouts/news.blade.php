<div class="col-sm-18 col-md-18 padding_left">
   <div class="news_column">
      @foreach ($tintuc as $new)
         <div class="panel panel-default1">
            <div class="panel-body featured">
               <a href="{{ url('tin-tuc/'.$new->ne_id.'-'.$new->ne_slug) }}.html" title="Chính sách hoàn tiền"><img  alt="Chính sách hoàn tiền" src="{{ asset('uploads/images/news/'.$new->ne_avatar) }}" width="300" class="img-thumbnail pull-left imghome" /></a>
               <h2>
                  <a href="{{ url('tin-tuc/'.$new->ne_id.'-'.$new->ne_slug) }}.html" title="{{ $new->ne_title }}">{{ $new->ne_title }}</a>
               </h2>
               <div class="text-muted">
                  <ul class="list-unstyled list-inline">
                     <li>
                        <em class="fa fa-clock-o">&nbsp;</em> {{ $new->ne_created_at }}
                     </li>
                  </ul>
               </div>
               <p>
                  {{ $new->ne_des }}
               </p>
            </div>
         </div>
      @endforeach
      
   </div>
</div>
<div class="clearfix col-md-12 col-xs-12">
    {{ $tintuc->links() }}
</div>