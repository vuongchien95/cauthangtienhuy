<div class="col-sm-18 col-md-18 padding_left">
    <div class="congtrinh">
        <div class="congtrinh-heading">
            SẢN PHẨM
            <div class="vach_duoi"></div>
        </div>
        <div class="congtrinh-body">
            <div class="hint"></div>
            <div class="others_product margin_bottom_b">
                @foreach($products as $prod)
                    <div class="col-xs-12 col-sm-12 col-md-8 text-center">
                        <div class="others_product_items">
                            <div class="others_product_img">
                                <a href="{{ url('/san-pham/'.$prod->pro_id.'-'.$prod->pro_slug) }}" title="{{ $prod->pro_title }}">
                                    <img src="{{ asset('uploads/images/Products/'.$prod->pro_avatar) }}" alt="{{ $prod->pro_title }}" class="img-thumbnail" width=""/>
                                </a>
                            </div>
                            <div class="others_product_title">
                                <a href="{{ url('/san-pham/'.$prod->pro_id.'-'.$prod->pro_slug) }}" title="{{ $prod->pro_title }}">
                                    {{ $prod->pro_title }}
                                </a>
                            </div>
                            <div class="others_product_price"> 
                                <span>{{ $prod->price }} đ</span>
                            </div>
                            <div class="dat_hang_sp">
                                <div class="a_detail">
                                    <a href="{{ url('/san-pham/'.$prod->pro_id.'-'.$prod->pro_slug) }}.html" title="{{ $prod->pro_title }}">Chi tiết</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>