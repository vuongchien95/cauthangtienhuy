<div class="clear"></div>
<div class="row">
    @foreach($products as $prod)
    <div class="col-xs-12 col-sm-12 col-md-8">
        <div class="others_product">
            <div class="others_product_items">
                <div class="others_product_img">
                    <a href="{{ url('/san-pham/'.$prod->pro_id.'-'.$prod->pro_slug) }}" title="{{ $prod->pro_title }}">
                        <img src="{{ asset('uploads/images/Products/'.$prod->pro_avatar) }}" alt="{{ $prod->pro_title }}" class="img-thumbnail" style="max-height:300px;max-width:300px;">
                    </a>
                </div>
                <div class="info_pro">
                </div>
                <div class="caption text-center">
                    <h3 class="others_product_title">
                        <a href="{{ url('/san-pham/'.$prod->pro_id.'-'.$prod->pro_slug) }}" title="{{ $prod->pro_title }}">
                            {{ $prod->pro_title }}
                        </a>
                    </h3>
                    <div class="others_product_price">
                        <span>{{ $prod->price }} đ</span> 
                    </div>
                    <div class="dat_hang_sp">
                        <div class="a_detail">
                            <a href="{{ url('/san-pham/'.$prod->pro_id.'-'.$prod->pro_slug) }}" title="">Chi tiết</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
<div class="clearfix col-md-12 col-xs-12">
    {{ $products->links() }}
</div>