<!-- Start Slider Area -->
<div class="intro-area intro-area-2">
    <div class="main-overly"></div>
    <div class="intro-carousel">
        <div class="intro-content">
            <div class="slider-images">
                <img src="img/slider/s3.jpg" alt="">
            </div>
        </div>
        <div class="intro-content">
            <div class="slider-images">
                <img src="img/slider/s4.jpg" alt="">
            </div>
{{--             <div class="slider-content">
                <div class="display-table">
                    <div class="display-table-cell">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <!-- layer 1 -->
                                    <div class="layer-1">
                                        <h1 class="title2">Developing the future<br/><span class="color">beyond limits</span></h1>
                                    </div>
                                    <!-- layer 2 -->
                                    <div class="layer-2 ">
                                        <p>Over 40 years of building trust and delivering excellence in the world.</p>
                                    </div>
                                    <!-- layer 3 -->
                                    <div class="layer-3">
                                        <a href="#" class="ready-btn left-btn">Our Services</a>
                                        <a href="#" class="ready-btn right-btn">Contact us</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
        </div>
    </div>
</div>
<!-- End Slider Area -->
<!-- end Brand Area -->
<div class="banner-area">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="banner-content">
          <h4>Bạn muốn mua sản phẩm đẹp, chất lượng</h4>
          <div class="banner-contact">
            <span class="call-us"><i class="icon icon-phone-handset"></i>Liên hệ : +4321-7654543</span><span>hoặc</span>
            <a class="banner-btn" href="#">Gửi thông tin</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>