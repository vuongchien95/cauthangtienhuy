@extends('FrontEnd.layouts.master')
@section('title', 'Chi tiết')
@section('keywords', 'tu van pqa, chuyen gia tu van pqa')
@section('description', 'Dược phẩm PQA chuyên sản xuất, kinh doanh dược phẩm, thực phẩm chức năng từ thảo dược thiên nhiên với các bài thuốc gia truyền, cổ truyền chữa bệnh chữa tận gốc...')
@section('main-content')
<div class="full-menu"></div>
<nav class="third-nav">
    <div class="wraper">
        <div class="row">
            <div class="bg">
                <div class="clearfix">
                    <div class="breadcrumbs-wrap">
                        <div class="display">
                            <a class="show-subs-breadcrumbs hidden" href="javascript:void(0);" onclick="showSubBreadcrumbs(this, event);"><em class="fa fa-lg fa-angle-right"></em></a>
                            <ul class="breadcrumbs list-none">
                                <li id="brcr_0" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                                    <a itemprop="url" href="{{ url('/') }}"><span itemprop="title">Trang nhất</span></a>
                                </li>
                                <li id="brcr_1" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                                    <a itemprop="url" href="{{ url('san-pham') }}.html"><span itemprop="title">Tin tức</span></a>
                                </li>
                                <li id="brcr_1" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                                    <span itemprop="title"></span>
                                </li>
                            </ul>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>
<div class="wraper">
    <section>
       <div class="container" id="body">
       		<div class="row">
            @include('FrontEnd.layouts.left_news')

            <div class="col-sm-18 col-md-18 padding_left">
              <div id="detail">
                <div class="panel-default" style="border:0">
                  <div class="panel-body">
                    <div class="row">
                      <div class="col-xs-24 col-sm-12 text-center">
                        <img src="{{ asset('uploads/images/Products/'.$products->pro_avatar) }}" alt="{{ $products->pro_title }}" class="img-responsive">
                      </div>
                      <div class="col-xs-24 col-sm-12 padding_left_20">
                        <ul class="product_info">
                          <li>
                            <h2>{{ $products->pro_title }}</h2>
                          </li>
                          <li>
                            <p>
                              Giá bán:
                              <span class="money">{{ $products->price }} đ</span>
                            </p>
                          </li>
                          
                        </ul>

                        <ul class="nv-social-share">
                          <li class="facebook">
                            <div class="fb-like fb_iframe_widget" data-href="http://bacsichuabenh.com/shops/chua-viem-xoang-mui-di-ung-doctor-ninh-dang-bot/chua-viem-xoang-mui-di-ung-doctor-ninh-dang-bot.html" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true" fb-xfbml-state="rendered" fb-iframe-plugin-query="action=like&amp;app_id=&amp;container_width=4&amp;href=http%3A%2F%2Fbacsichuabenh.com%2Fshops%2Fchua-viem-xoang-mui-di-ung-doctor-ninh-dang-bot%2Fchua-viem-xoang-mui-di-ung-doctor-ninh-dang-bot.html&amp;layout=button_count&amp;locale=vi_VN&amp;sdk=joey&amp;share=true&amp;show_faces=false"><span style="vertical-align: bottom; width: 122px; height: 20px;"><iframe name="f116143eb750af4" width="1000px" height="1000px" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" title="fb:like Facebook Social Plugin" src="https://www.facebook.com/plugins/like.php?action=like&amp;app_id=&amp;channel=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FFdM1l_dpErI.js%3Fversion%3D42%23cb%3Df2450e8db22bf2c%26domain%3Dbacsichuabenh.com%26origin%3Dhttp%253A%252F%252Fbacsichuabenh.com%252Ff1b34348148b668%26relation%3Dparent.parent&amp;container_width=4&amp;href=http%3A%2F%2Fbacsichuabenh.com%2Fshops%2Fchua-viem-xoang-mui-di-ung-doctor-ninh-dang-bot%2Fchua-viem-xoang-mui-di-ung-doctor-ninh-dang-bot.html&amp;layout=button_count&amp;locale=vi_VN&amp;sdk=joey&amp;share=true&amp;show_faces=false" style="border: none; visibility: visible; width: 122px; height: 20px;" class=""></iframe></span></div>
                          </li>
                          <li>
                            <div id="___plusone_0" style="text-indent: 0px; margin: 0px; padding: 0px; background: transparent; border-style: none; float: none; line-height: normal; font-size: 1px; vertical-align: baseline; display: inline-block; width: 32px; height: 20px;"><iframe ng-non-bindable="" frameborder="0" hspace="0" marginheight="0" marginwidth="0" scrolling="no" style="position: static; top: 0px; width: 32px; margin: 0px; border-style: none; left: 0px; visibility: visible; height: 20px;" tabindex="0" vspace="0" width="100%" id="I0_1524133721581" name="I0_1524133721581" src="https://apis.google.com/u/0/se/0/_/+1/fastbutton?usegapi=1&amp;size=medium&amp;hl=vi&amp;origin=http%3A%2F%2Fbacsichuabenh.com&amp;url=http%3A%2F%2Fbacsichuabenh.com%2Fshops%2Fchua-viem-xoang-mui-di-ung-doctor-ninh-dang-bot%2Fchua-viem-xoang-mui-di-ung-doctor-ninh-dang-bot.html&amp;gsrc=3p&amp;ic=1&amp;jsh=m%3B%2F_%2Fscs%2Fapps-static%2F_%2Fjs%2Fk%3Doz.gapi.vi.Y8LODLRxIyY.O%2Fm%3D__features__%2Fam%3DAQE%2Frt%3Dj%2Fd%3D1%2Frs%3DAGLTcCPwuuKCCbKfDLplQ2VPEOGo-NZWcw#_methods=onPlusOne%2C_ready%2C_close%2C_open%2C_resizeMe%2C_renderstart%2Concircled%2Cdrefresh%2Cerefresh%2Conload&amp;id=I0_1524133721581&amp;_gfid=I0_1524133721581&amp;parent=http%3A%2F%2Fbacsichuabenh.com&amp;pfname=&amp;rpctoken=28638658" data-gapiattached="true" title="G+"></iframe></div>
                          </li>
                          <li>
                            <iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" class="twitter-share-button twitter-share-button-rendered twitter-tweet-button" title="Twitter Tweet Button" src="https://platform.twitter.com/widgets/tweet_button.8a07f6b35a2c420c46c7b6ad0cc3b536.vi.html#dnt=false&amp;id=twitter-widget-0&amp;lang=vi&amp;original_referer=http%3A%2F%2Fbacsichuabenh.com%2Fshops%2Fchua-viem-xoang-mui-di-ung-doctor-ninh-dang-bot%2Fchua-viem-xoang-mui-di-ung-doctor-ninh-dang-bot.html&amp;size=m&amp;text=Ch%E1%BB%AFa%20Vi%C3%AAm%20Xoang%20M%C5%A9i%20-%20D%E1%BB%8B%20%E1%BB%A8ng%20Doctor%20Ninh%20(d%E1%BA%A1ng%20b%E1%BB%99t)&amp;time=1524133721372&amp;type=share&amp;url=http%3A%2F%2Fbacsichuabenh.com%2Fshops%2Fchua-viem-xoang-mui-di-ung-doctor-ninh-dang-bot%2Fchua-viem-xoang-mui-di-ung-doctor-ninh-dang-bot.html" style="position: static; visibility: visible; width: 60px; height: 20px;"></iframe>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-24">
                  <div class="tabs_new">
                    <ul class="nav-tabs_new">
                      <li role="presentation" class="active">
                        <a href="#content_detail-1" aria-controls="content_detail-1" role="tab" data-toggle="tab">
                        <span>Thông tin sản phẩm</span>
                        </a>
                      </li>
                    </ul>
                    <div class="tab-content">
                      <div role="tabpanel" class="tab-pane fade active in" id="content_detail-1">
                        {!! $products->pro_content !!}
                      </div>
                    </div>
                  </div>
                  {{-- <div class="panel panel-default">
                    <div class="panel-body">
                      <div class="keywords">
                        <em class="fa fa-tags">&nbsp;</em><strong>Từ khóa: </strong>
                        <a title="chua viem xoang mui" href="/shops/tag/chua-viem-xoang-mui"><em>chua viem xoang mui</em></a>, 
                        <a title="chua viem mui di ung" href="/shops/tag/chua-viem-mui-di-ung"><em>chua viem mui di ung</em></a>
                      </div>
                    </div>
                  </div> --}}
                </div>
            
              </div>


            </div>
       		</div>
       	</div>
    </section>
</div>
@endsection