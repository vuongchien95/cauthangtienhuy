@extends('FrontEnd.layouts.new-master')
@section('title', 'Giới thiệu')
@section('keywords', 'tu van pqa, chuyen gia tu van pqa')
@section('description', 'Dược phẩm PQA chuyên sản xuất, kinh doanh dược phẩm, thực phẩm chức năng từ thảo dược thiên nhiên với các bài thuốc gia truyền, cổ truyền chữa bệnh chữa tận gốc...')
@section('content')
<!-- Start Bottom Header -->
<div class="page-area">
    <div class="breadcumb-overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="breadcrumb text-center">
                    <div class="section-headline white-head text-center">
                        <h3>About us</h3>
                    </div>
                    <ul>
                        <li class="home-bread">Home</li>
                        <li>About us</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- about-area start -->
        <div class="about-area area-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="about-content">
                            <h4>We are always clients satisfaction who need there.</h4>
                            <p>The phrasal sequence of the Lorem Ipsum text is now so widespread and commonplace that many DTP programmes can generate dummy text using the starting sequence "Lorem ipsum". Fortunately, the phrase 'Lorem Ipsum' is now recognized by electronic pre-press systems and, when found, an alarm can be raised.</p>
                            <div class="about-details text-center">
                                <div class="single-about">
                                    <a href="#"><i class="icon icon-diamond"></i></a>
                                    <h5>Certified company</h5>
                                </div>
                                <div class="single-about">
                                    <a href="#"><i class="icon icon-star"></i></a>
                                    <h5>Clients satisfaction</h5>
                                </div>
                                <div class="single-about">
                                    <a href="#"><i class="icon icon-users"></i></a>
                                    <h5>Experience employee</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="video-inner text-center">
                            <div class="video-content">
                                <a href="https://www.youtube.com/watch?v=O33uuBh6nXA" class="video-play vid-zone">
                                <i class="fa fa-play"></i>
                            </a>
                            </div>
                        </div>
                    </div>
                    <!-- column end -->
                </div>
            </div>
        </div>
        <!-- about-area end -->
@endsection