@extends('FrontEnd.layouts.master')
@section('title', 'Cầu thang Tiến Huy')
@section('keywords', 'Cầu Thang Tiến Huy')
@section('description', 'Cầu Thang Tiến Huy cam kết sẽ mang đến những sản phẩm hoàn hảo cho ngôi nhà của bạn.')
@section('content')
    {{-- Slider --}}
    <style type="text/css">
        @media(min-width: 1000px)
        {
            .services-img img {
                min-height: 282px;
            }
            .box-item-pr {
                height: 500px;
                overflow: hidden;
            }
        }
    </style>
    @include('FrontEnd.layouts.slider')
   
    @php
        $getCate = DB::table('category')->get();
        foreach ($getCate as $key => $ct)
        {
        @endphp
            <div class="welcome-area area-padding">
                <div class="container">
                    <div class="row">
                        <div class="well-main-services">
                            <div class="col-md-12">
                                <h2>{{ $ct->cat_name }}</h2>
                            </div>
                            
                            @php
                                $getProduct = DB::table('products')->where('pro_cate', $ct->cat_id)->get();
                                foreach ($getProduct as $pro) {
                            @endphp
                            <div class="col-md-4 col-sm-4 col-xs-12 box-item-pr">
                                <div class="well-services">
                                    <div class="services-img">
                                        <img src="{{ asset('uploads/images/Products/'.$pro->pro_avatar) }}" alt="{{ $pro->pro_title }}">
                                    </div>
                                    <div class="main-services">
                                        <div class="service-content">
                                            <h4>{{ $pro->pro_title }}</h4>
                                            <p>
                                                <span>Giá:
                                                    @if($pro->price != '')
                                                        @if($pro->new_price != '')
                                                            <label style="color: #f33;font-size: 18px;">{{ number_format($pro->new_price,0,",",".") }} đ</label>
                                                            <span style="text-decoration: line-through;">{{ number_format($pro->price,0,",",".") }} đ</span>
                                                        @else 
                                                            <label>{{ $pro->new_price }}</label>
                                                        @endif
                                                    @endif
                                                </span>
                                            </p>
                                            <a class="service-btn" href="tel:0963.516.833">Liên hệ: <span style="color: #f33;font-size: 18px;">0963.516.833</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @php
                                }
                            @endphp
                        </div>
                    </div>
                </div>
            </div>
        @php
        }
    @endphp
    
	

    <!-- BẢng giá -->
    <div class="services-area area-padding">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="section-headline text-center">
              <h3>Bảng giá cầu thang</h3>
              <p>Construct is an engineering and general contracting group that has gained its expertise over the past twenty- six years.</p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="services-all">
            
            <!-- Start services -->
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="single-services">
                <a class="service-images" href="#"><i class="flaticon-architecture-draw"></i></a>
                <div class="service-content">
                  <h4><a href="#">Cầu thang gỗ</a></h4>
                  <p>Tay vịn gỗ:</p>
                  <ul style="list-style-type: square;">
                  	<li>Đơn giá: <span>1.200.000- </span><span>1.600.000</span><sup>đ</sup></li>
                  	<li>Hệ cong: 1.5</li>
                  	<li>Đơn vị: mét</li>
                  	<li>Tay 60-65 gỗ lim Nam Phi</li>
                  	<br>
                  </ul>
                  <hr>
                  <p>Mặt bậc gỗ:</p>
                  <ul style="list-style-type: square;">
                  	<li>Đơn giá: <span>850.000</span><sup>đ</sup></li>
                  	<li>Hệ cong: 1.5</li>
                  	<li>Đơn vị: mét</li>
                  	<li>Tay 60-65 gỗ lim Nam Phi, suốt phi 14-16 dày 1,2mm, chân con tiện lặp là bản 40x5mm.</li>
                  </ul>
                  <hr>
                  <p>Trụ gỗ:</p>
                  <ul style="list-style-type: square;">
                  	<li>Đơn giá: <span>650.000</span><sup>đ</sup></li>
                  	<li>Hệ cong: 1.5</li>
                  	<li>Đơn vị: mét</li>
                  	<li>Tay vịn sắt 60, suốt phi 14-16 dày 1,2mm, chân con tiện lặp là bản 40x5mm</li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- Start services -->
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="single-services">
                <a class="service-images" href="#"><i class="flaticon-architecture-draw"></i></a>
                <div class="service-content">
                  <h4><a href="#">Cầu thang kính</a></h4>
                  <p>Cầu thang kính bát đơn:</p>
                  <ul style="list-style-type: square;">
                  	<li>Đơn giá: <span>1.700.000</span><sup>đ</sup></li>
                  	<li>Hệ cong: 1.5</li>
                  	<li>Đơn vị: mét</li>
                  	<li>Tay vịn inox 60, suốt inox Hoàng Vũ D=12,7, dày 0,7mm, chân con tiện inox nhập khẩu 2 nẹp trang trí.</li>
                  </ul>
                  <hr>
                  <p>Cầu thang kính chân lửng:</p>
                  <ul style="list-style-type: square;">
                  	<li>Đơn giá: <span>1.500.000</span><sup>đ</sup></li>
                  	<li>Hệ cong: 1.5</li>
                  	<li>Đơn vị: mét</li>
                  	<li>Chân con tiện inox lửng, tay 60-65 gỗ lim Nam Phi, kính 10-12mm Hải Long</li>
                  </ul>
                  <hr>
                  <p>Cầu thang kính chân cao:</p>
                  <ul style="list-style-type: square;">
                  	<li>Đơn giá: <span>1.250.000</span><sup>đ</sup></li>
                  	<li>Hệ cong: 1.5</li>
                  	<li>Đơn vị: mét</li>
                  	<li>Chân con tiện inox nhập khẩu có 2 nẹp trang trí, tay 60-65 gỗ lim Nam Phi, kính 10-12mm Hải Long.</li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- Start services -->
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="single-services">
                <a class="service-images" href="#"><i class="flaticon-architecture-draw"></i></a>
                <div class="service-content">
                  <h4><a href="#">Cầu thang sắt</a></h4>
                  <p>Sắt nghệ thuật:</p>
                  <ul style="list-style-type: square;">
                  	<li>Đơn giá: <span>1.200.000</span>-<span>1.600.000</span><sup>đ</sup></li>
                  	<li>Hệ cong: 1.5</li>
                  	<li>Đơn vị: mét</li>
                  	<li>Tay 60-65 gỗ lim Nam Phi.</li>
                  </ul>
                  <hr>
                  <p>Sắt thường tay vịn gỗ:</p>
                  <ul style="list-style-type: square;">
                  	<li>Đơn giá: <sapn>850.000</sapn><sup>đ</sup></li>
                  	<li>Hệ cong: 1.5</li>
                  	<li>Đơn vị: mét</li>
                  	<li>Tay 60-65 gỗ lim Nam Phi, suốt phi 14-16 dày 1,2mm, chân con tiện lặp là bản 40x5mm.</li>
                  </ul>
                  <hr>
                  <p>Tay vịn sắt thường:</p>
                  <ul style="list-style-type: square;">
                  	<li>Đơn giá: <sapn>650.000</sapn><sup>đ</sup></li>
                  	<li>Hệ cong: 1.5</li>
                  	<li>Đơn vị: mét</li>
                  	<li>Tay vịn sắt 60, suốt phi 14-16 dày 1,2mm, chân con tiện lặp là bản 40x5mm.</li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- Start services -->
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="single-services">
                <a class="service-images" href="#"><i class="flaticon-crane-tool"></i></a>
                <div class="service-content">
                  <h4><a href="#">Cầu thang inox</a></h4>
                  <p>Cầu thang inox tay vịn gỗ:</p>
                  <ul style="list-style-type: square;">
                  	<li>Đơn giá: <sapn>800.000</sapn><sup>đ</sup></li>
                  	<li>Hệ cong: 1.5</li>
                  	<li>Đơn vị: mét</li>
                  	<li>Tay 60-65 gỗ lim Nam Phi, suốt inox Hoàng Vũ D=12,7, dày 0,7mm, chân con tiện inox nhập khẩu 2 nẹp trang trí.</li>
                  </ul>
                  <hr>
                  <p>Cầu thang inox tay vịn inox
                  <ul style="list-style-type: square;">
                  	<li>Đơn giá: <sapn>850.000</sapn><sup>đ</sup></li>
                  	<li>Hệ cong: 1.5</li>
                  	<li>Đơn vị: mét</li>
                  	<li>Tay vịn inox 60, suốt inox Hoàng Vũ D=12,7, dày 0,7mm, chân con tiện inox nhập khẩu 2 nẹp trang trí.</li>
                  </ul>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
    <!-- End bang gia area -->

    <!--Blog Area Start-->
    <div class="blog-area area-padding">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="section-headline text-center">
              <h3>Tin tức</h3>
              <p>Construct is an engineering and general contracting group that has gained its expertise over the past twenty- six years.</p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="blog-grid home-blog">
            <!-- Start single blog -->
            <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="single-blog">
                <div class="blog-image">
                  <a class="image-scale" href="#">
                  <img src="img/blog/1.jpg" alt="">
                  </a>
                </div>
                <div class="blog-content">
                  <div class="blog-title">
                    <div class="blog-meta">
                      <span class="date-type">
                      20 june, 2017
                      </span>
                      <span class="comments-type">
                      <i class="fa fa-comment-o"></i>
                      21
                      </span>
                    </div>
                    <a href="#">
                      <h4>Residential house location</h4>
                    </a>
                  </div>
                  <div class="blog-text">
                    <p>Redug Lagre dolor sit amet, consectetur adipisicing elit. Minima in nostrum, veniam. Esse est assumenda inventore.</p>
                    <a class="blog-btn" href="#">Read more</a>
                  </div>
                </div>
              </div>
            </div>
            <!-- End single blog -->
            <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="single-blog">
                <div class="blog-image">
                  <a class="image-scale" href="#">
                  <img src="img/blog/2.jpg" alt="">
                  </a>
                </div>
                <div class="blog-content">
                  <div class="blog-title">
                    <div class="blog-meta">
                      <span class="date-type">
                      14 Nov, 2017
                      </span>
                      <span class="comments-type">
                      <i class="fa fa-comment-o"></i>
                      12
                      </span>
                    </div>
                    <a href="#">
                      <h4>Welding working is perfect</h4>
                    </a>
                  </div>
                  <div class="blog-text">
                    <p>Redug Lagre dolor sit amet, consectetur adipisicing elit. Minima in nostrum, veniam. Esse est assumenda inventore.</p>
                    <a class="blog-btn" href="#">Read more</a>
                  </div>
                </div>
              </div>
            </div>
            <!-- End single blog -->
            <div class="col-md-4 col-sm-12 col-xs-12">
              <div class="single-blog">
                <div class="blog-image">
                  <a class="image-scale" href="#">
                  <img src="img/blog/3.jpg" alt="">
                  </a>
                </div>
                <div class="blog-content">
                  <div class="blog-title">
                    <div class="blog-meta">
                      <span class="date-type">
                      12 Dec, 2017
                      </span>
                      <span class="comments-type">
                      <i class="fa fa-comment-o"></i>
                      13
                      </span>
                    </div>
                    <a href="#">
                      <h4>Electrical system development</h4>
                    </a>
                  </div>
                  <div class="blog-text">
                    <p>Redug Lagre dolor sit amet, consectetur adipisicing elit. Minima in nostrum, veniam. Esse est assumenda inventore.</p>
                    <a class="blog-btn" href="#">Read more</a>
                  </div>
                </div>
              </div>
            </div>
            <!-- End single blog -->
          </div>
        </div>
        <!-- End row -->
      </div>
    </div>
    <!--End of Blog Area-->
@endsection
