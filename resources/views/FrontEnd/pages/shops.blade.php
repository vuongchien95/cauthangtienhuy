@extends('FrontEnd.layouts.master')
@section('title', ' ')
@section('keywords', ' ')
@section('description', ' ')
@section('main-content')
<div class="full-menu"></div>
<nav class="third-nav">
    <div class="wraper">
        <div class="row">
            <div class="bg">
                <div class="clearfix">
                    <div class="breadcrumbs-wrap">
                        <div class="display">
                            <a class="show-subs-breadcrumbs hidden" href="javascript:void(0);" onclick="showSubBreadcrumbs(this, event);"><em class="fa fa-lg fa-angle-right"></em></a>
                            <ul class="breadcrumbs list-none">
                                <li id="brcr_0" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="/"><span itemprop="title">Trang nhất</span></a></li>
                                <li id="brcr_1" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="/lien-he.html"><span itemprop="title">Liên hệ</span></a></li>
                            </ul>
                        </div>
                        <ul class="subs-breadcrumbs"></ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>
<div class="wraper">
    <section>
       <div class="container" id="body">
       		<div class="row">
            @include('FrontEnd.layouts.left_news')
            <div class="col-sm-18 col-md-18 padding_left">
                    <div id="category">
                        <div class="page-header">
                            <h1>Tất cả sản phẩm</h1>
                        </div>
                        @include('FrontEnd.layouts.shops')
                        </div>
                    </div>
                </div>
       		</div>
       	</div>
    </section>
</div>
@endsection