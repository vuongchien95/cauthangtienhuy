@extends('FrontEnd.layouts.master')
@section('title', 'Tin tức')
@section('keywords', 'Công ty cổ phần dược phẩm PQA, dược phẩm PQA, công ty PQA, cong ty co phan duoc pham pqa, duoc pham pqa')
@section('description', 'Dược phẩm PQA chuyên sản xuất, kinh doanh dược phẩm, thực phẩm chức năng từ thảo dược thiên nhiên với các bài thuốc gia truyền, cổ truyền chữa bệnh chữa tận gốc...')
@section('main-content')
<div class="full-menu"></div>
<nav class="third-nav">
    <div class="wraper">
        <div class="row">
            <div class="bg">
                <div class="clearfix">
                    <div class="breadcrumbs-wrap">
                        <div class="display">
                            <a class="show-subs-breadcrumbs hidden" href="javascript:void(0);" onclick="showSubBreadcrumbs(this, event);"><em class="fa fa-lg fa-angle-right"></em></a>
                            <ul class="breadcrumbs list-none">
                                <li id="brcr_0" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="{{ url('/') }}"><span itemprop="title">Trang nhất</span></a></li>
                                <li id="brcr_1" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="{{ url('tin-tuc') }}.html"><span itemprop="title">Tin tức</span></a></li>
                            </ul>
                        </div>
                        <ul class="subs-breadcrumbs"></ul>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>
<div class="wraper">
    <section>
       <div class="container" id="body">
       		<div class="row">
            @include('FrontEnd.layouts.left_news')
            @include('FrontEnd.layouts.news')
       		</div>
       	</div>
    </section>
</div>
@endsection