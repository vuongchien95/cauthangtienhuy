<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link rel="stylesheet" href="{{ asset('backend/css/login.css') }}">
    <div class="container">
    <h1 class="welcome text-center"> Maxgo.vn </h1>
        <div class="card card-container">
        <h2 class='login_title text-center'>Login</h2>
        @if(Session::has('error'))
            <div class="text-center">
                <span style="color: red">{{ Session::get('error') }}</span>
            </div>
        @endif
        <hr>
            <form class="form-signin" method="post">
                {{ csrf_field() }}
                <span id="reauth-email" class="reauth-email"></span>
                <p class="input_title">Email</p>
                <input type="email" id="inputEmail" name="email" class="login_box" required autofocus>
                <p class="input_title">Password</p>
                <input type="password" id="inputPassword" name="password" class="login_box" required>
                <div id="remember" class="checkbox" style="padding-left: 20px">
                    <input type="checkbox" name="remember"> Remember me
                </div>
                <button class="btn btn-lg btn-primary" type="submit">Login</button>
            </form><!-- /form -->
        </div><!-- /card-container -->
    </div><!-- /container -->
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
