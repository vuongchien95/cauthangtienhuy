<?php

@include('route_admin.php');

/*
|--------------------------------------------------------------------------
| Front-End
|--------------------------------------------------------------------------
|
| 
|
*/
Route::group(['prefix' => '/', 'namespace' => 'FrontEnd'], function() {
	
    Route::get('/', 'PagesController@home');
    
    Route::get('/lien-he.html', 'PagesController@lienhe');
    Route::get('/tin-tuc.html', 'PagesController@tintuc');
    Route::get('/tin-tuc/{id}-{ne_slug}.html', 'PagesController@detailtintuc');
    Route::get('/gioi-thieu.html', 'PagesController@gioithieu');
    Route::get('/hoi-dong-khoa-hoc.html', 'PagesController@khoahoc');

    Route::get('/san-pham.html', 'PagesController@shops');
    Route::get('/san-pham/{id_pro}-{slug_pro}.html', 'PagesController@detailProduct');
    Route::get('/danh-muc/{id_dm}-{slug_dm}.html', 'PagesController@byCate');

});